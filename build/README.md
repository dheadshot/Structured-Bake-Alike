% Structured Bake-Alike README
% DHeadshot

# Structured Bake-Alike

Based on [bake](https://github.com/fcanas/bake) by Fabian Canas.

A static site generator that's (mostly) just [Pandoc](http://pandoc.org) and [Make](https://www.gnu.org/software/make/), though some tools are added to make it work on Windows.

The original Bake by Fabian Canas wasn't a CMS or a blog engine - it simply let you publish a static site written in markdown, or manual html, or whatever.

Structured Bake-Alike adds a very simple blog engine (that you can happily ignore if you prefer) and places the files in more of a structure (basically, everything to build has been moved to the "build" directory to stop it getting mixed up with the templates - now in the template directory - and the constructors).

The `Makefile` is most of the relevant code. The build directory contains mainly a description of [this site](http://fcanas.github.io/bake) from the original "Bake", with the addition of some blog entries.  The correct generation of the [the Bake site](http://fcanas.github.io/bake) is evidence of Bake working and Structured Bake-Alike differs only slightly. A mechanism for automated testing of the product isn't planned, but definitely possible.

## Use

Your best option is probably to clone this repository and replace the files in the "build" directory with your own, edit the files in the "template" directory to match how you wish your site to be rendered, then if you wish to use GitHub Pages for deployment or simply Git to track the site, delete the `.git/` directory and do a fresh `git init`.

```bash
git clone https://codeberg.org/DHeadshot/Structured-Bake-Alike.git
rm -r .git
git init
```

### General Usage

```bash
make [OPTIONS] [COMMAND]
```

#### Options for Make

- `OSSYSTEM=WINDOWS` sets the Makefile to use Windows paths and commands (default)
- `OSSYSTEM=MACOS` or `OSSYSTEM=LINUX` sets the Makefile to use Unix paths and commands
- `"SITETITLE=Title of the Website"` sets the Title of the Website
- `"SITEMENU_HTML=./menu.html"` Sets the path of the top Menu HTML file.  Use this OR the `SITEMENU_MD` option but NOT BOTH.
- `"SITEMENU_MD=./menu.md"` Sets the path of the top Menu markdown file.  Use this OR the `SITEMENU_HTML` option but NOT BOTH.
- `"FOOTER_HTML=./footer.html"` Sets the path of the footer HTML file.  Use this OR the `FOOTER_MD` option but NOT BOTH.
- `"FOOTER_MD=./footer.md"` Sets the path of the footer markdown file.  Use this OR the `FOOTER_HTML` option but NOT BOTH.
- `LEGACY=1` Sets Pandoc to use the legacy form of Github Markdown (default)
- `LEGACY=0` Sets Pandoc to use the modern form of Github Markdown, which may be incompatible with other parts of the Makefile.

#### Commands for Make

- `bake` (or none): Build the website to the "deploy" directory.
- `clean`: Wipe the "deploy" directory for a fresh build.
- `new-blog`: Create a new blog entry.
- `deploy`: Deploy to GitHub Pages using the original "Bake" scripts.  May need editing.  Probably won't work on Windows.
- `undeploy`: Roll back the deployment from GitHub pages using the original "Bake" scripts.  Unix-like systems only.

For `deploy` and `undeploy`, the Makefile has a recipe to push a git subtree to GitHub pages if your remote is setup as `github`. You can edit that area of the Makefile if that doesn't match your configuration.

## Directories

- `build`: The directory into which the website content source files are placed (apart from the source markdown for the blog page).  This also contains the CSS files.
- `deploy`: The directory into which the website is generated.
- `templates`: The directory into which the HTML templates are placed.  These structure the page.  This also contains the source markdown for the blog page.

The "build" directory contains the following directories:

- `blog`: Blog entries go in here
- `images`: Put images in here.  Keeps things tidy and ensures they are all copied across verbatim.
- `projects`: An example subdirectory.  Delete this or replace it with whatever you like.
- `uploads`: Miscellaneous files that need to be copied across verbatim.


## Dependencies

Have [Make](https://www.gnu.org/software/make/) on your system.

Install [Pandoc](http://pandoc.org)

See [Pandoc's installation instructions](http://pandoc.org/installing.html) or try (if on a Mac):

```bash
brew install pandoc
```

### Windows-Only Dependencies

This repository contains compiled versions of the Windows programs that replace standard Unix utilities to make this work on Windows, but in case they are needed, these can be found below:

- [isodate.exe](https://codeberg.org/dheadshot/ISODate): used in place of `date "+%04Y-%02m-%02d %02H:%02M:%02S.000"`.
- [psearch.exe](https://codeberg.org/dheadshot/PatternSearch): used in place of `find`.
- [getlines.exe](https://codeberg.org/dheadshot/GetLines): used in place of `cat <file> | sed -n 1p`.
- [makdir.exe](https://codeberg.org/dheadshot/MakDir): used in place of `mkdir`
- [rffws.exe](https://codeberg.org/dheadshot/RenameFilesForWebSite): used in place of `find` with the enclosed `rfws.sh` shell script.

## Structured Bake-Alike Makefile Source

```makefile
# Check what OS we are running and set SYSTEM to WIN or NIX accordingly.
# (Default to WIN otherwise)
ifdef OSSYSTEM
ifeq ($(OSSYSTEM),WINDOWS)
SYSTEM = WIN
else ifeq ($(OSSYSTEM),WIN32)
SYSTEM = WIN
else ifeq ($(OSSYSTEM),WIN64)
SYSTEM = WIN
else ifeq ($(OSSYSTEM),MACOS)
SYSTEM = NIX
else ifeq ($(OSSYSTEM),OSX)
SYSTEM = NIX
else ifeq ($(OSSYSTEM),LINUX)
SYSTEM = NIX
else ifeq ($(OSSYSTEM),BSD)
SYSTEM = NIX
else ifeq ($(OSSYSTEM),UNIX)
SYSTEM = NIX
else
SYSTEM = WIN
endif
else
SYSTEM = WIN
endif

# Filesystem Separator.  Doesn't always work using this macro in Windows, sadly.
ifeq ($(SYSTEM),NIX)
SEP = /
else
SEP = \\
endif


# Files
TEMPLATE = .$(SEP)templates$(SEP)template.tmp
TEMPLATE_MENU = .$(SEP)templates$(SEP)MenuTemplate.tmp
TEMPLATE_BLOG_MENU = .$(SEP)templates$(SEP)BlogMenuTemplate.tmp
TEMPLATE_FOOTER = .$(SEP)templates$(SEP)FooterTemplate.tmp
TEMPLATE_BLOG = .$(SEP)templates$(SEP)blogtemplate.tmp
# Don't use the directory prefix on the CSS!
CSS = writ.min.css

DEPLOY = deploy
BLOG_ENTRY_LIST = blog_entries.md
#.html

# Directories
BUILD_DIRECTORY_NIX = ./build/
BUILD_DIRECTORY_WIN = .\build

UPLOAD_DIRECTORY_NIX = $(BUILD_DIRECTORY_NIX)uploads/
UPLOAD_DIRECTORY_WIN = $(BUILD_DIRECTORY_WIN)\uploads

IMAGES_DIRECTORY_NIX = $(BUILD_DIRECTORY_NIX)images/
IMAGES_DIRECTORY_WIN = $(BUILD_DIRECTORY_WIN)\images

# Deploy directory.
# Excluded from source search. Prepended to all output files
DEPLOY_DIRECTORY_NIX = ./$(DEPLOY)/
DEPLOY_DIRECTORY_WIN = .\$(DEPLOY)

# Source control directory, also excluded from source search
SRC_CTL_NIX = ./.git%
SRC_CTL_WIN = .\.git%


ifeq ($(SYSTEM),NIX)
BUILD_DIRECTORY = $(BUILD_DIRECTORY_NIX)
UPLOAD_DIRECTORY = $(UPLOAD_DIRECTORY_NIX)
IMAGES_DIRECTORY = $(IMAGES_DIRECTORY_NIX)
DEPLOY_DIRECTORY = $(DEPLOY_DIRECTORY_NIX)
SRC_CTL = $(SRC_CTL_NIX)
else
BUILD_DIRECTORY = $(BUILD_DIRECTORY_WIN)
UPLOAD_DIRECTORY = $(UPLOAD_DIRECTORY_WIN)
IMAGES_DIRECTORY = $(IMAGES_DIRECTORY_WIN)
DEPLOY_DIRECTORY = $(DEPLOY_DIRECTORY_WIN)
SRC_CTL = $(SRC_CTL_WIN)
endif



# Programs:
PANDOC = pandoc
ISODATE = .\isodate.exe
PSEARCH = .\psearch.exe
GETLINES = .\getlines.exe
MAKDIR = .\makdir.exe
RFFWS = .\rffws.exe
RFWS = ./rfws.sh

# Set site options
ifdef SITETITLE
SITETITLEDECLARATION = --title-prefix="$(SITETITLE)"
else
SITETITLEDECLARATION = 
endif

ifdef SITEMENU_HTML
SITEMENUDECLARATION = --include-before-body="$(SITEMENU_HTML)"
else
ifdef SITEMENU_MD
ifeq ($(SYSTEM),NIX)
SITEMENU_MDHTML = $(subst $(BUILD_DIRECTORY_NIX)/,,$(patsubst %.md,%.html,$(SITEMENU_MD)))
else
SITEMENU_MDHTML = $(subst $(BUILD_DIRECTORY_WIN)\,,$(patsubst %.md,%.html,$(SITEMENU_MD)))
endif
SITEMENUDECLARATION = --include-before-body="$(SITEMENU_MDHTML)"
else
SITEMENUDECLARATION = 
endif
endif

ifdef FOOTER_HTML
FOOTERDECLARATION = --include-after-body="$(FOOTER_HTML)"
else
ifdef FOOTER_MD
ifeq ($(SYSTEM),NIX)
FOOTER_MDHTML = $(subst $(BUILD_DIRECTORY_NIX)/,./,$(patsubst %.md,%.html,$(FOOTER_MD)))
else
FOOTER_MDHTML = $(subst $(BUILD_DIRECTORY_WIN)\,.\,$(patsubst %.md,%.html,$(FOOTER_MD)))
endif
FOOTERDECLARATION = --include-after-body="$(FOOTER_MDHTML)"
else
FOOTERDECLARATION = 
endif
endif

# Pandoc legacy compensator - if Pandoc doesn't compile, try `make LEGACY=0`, though as gfm isn't compatible with pandoc_title_block, you may be stuck.
ifneq ($(LEGACY),0)
GITHUBMD = markdown_github
else
GITHUBMD = gfm
endif

# Rule for converting github flavored markdown to html5
MARKDOWN := $(PANDOC) --template $(TEMPLATE) -c $(CSS) --from $(GITHUBMD)+pandoc_title_block --to html5 $(SITETITLEDECLARATION) $(SITEMENUDECLARATION) $(FOOTERDECLARATION)  --standalone 
MARKDOWN_NS_MENU := $(PANDOC) --template $(TEMPLATE_MENU) --from $(GITHUBMD)+pandoc_title_block --to html5
MARKDOWN_NS_BLOGMENU := $(PANDOC) --template $(TEMPLATE_BLOG_MENU) --from $(GITHUBMD)+pandoc_title_block --to html5
MARKDOWN_NS_FOOTER := $(PANDOC) --template $(TEMPLATE_FOOTER) --from $(GITHUBMD)+pandoc_title_block --to html5
MARKDOWN_BLOGINDEX := $(PANDOC) --template $(TEMPLATE_BLOG) -c $(CSS) --from $(GITHUBMD)+pandoc_title_block --to html5 $(SITETITLEDECLARATION) $(SITEMENUDECLARATION) $(FOOTERDECLARATION)  --standalone 

# Map a function that takes two arguments
map = $(foreach a,$(3),$(call $(1),$(2),$(a)))

# Find Functions
find_all_nix = $(shell find $(BUILD_DIRECTORY_NIX) -type f -name $(1))
find_all_win = $(shell $(PSEARCH) $(BUILD_DIRECTORY_WIN) $(1) f)
find_uploads_nix = $(shell find $(BUILD_DIRECTORY_NIX)/uploads/ -type f -name $(1))
find_uploads_win = $(shell $(PSEARCH) $(BUILD_DIRECTORY_WIN)\uploads $(1) f)
ifeq ($(SYSTEM),NIX)
find_all = $(call find_all_nix,$(1))
find_uploads = $(call find_uploads_nix,$(1))
else
find_all = $(call find_all_win,$(1))
find_uploads = $(call find_uploads_win,$(1))
endif

# List functions
find_deploydir_win = $(shell cmd /c "dir /b $(DEPLOY_DIRECTORY_WIN)\$(1)")
find_deploydir_nix = $(shell ls -1 -d $(DEPLOY_DIRECTORY_NIX)/(1))

# RM Commands
RM_DIRS_WIN = RD /S /Q $(foreach a,$(call find_deploydir_win,*.),$(addprefix $(DEPLOY_DIRECTORY_WIN)\,$(a)))
# The `rm` command removes directories too, so we don't need a "RM_DIRS_NIX", but if we did, it would be  = rm -rf $(foreach a,$(call find_deploydir_nix,*/),$(addprefix $(DEPLOY_DIRECTORY_NIX)/,$(a)))
RM_ALL_NIX = rm -rf $(DEPLOY_DIRECTORY)*
RM_ALL_WIN = cmd.exe /C "del ^"$(DEPLOY_DIRECTORY_WIN)\*^" &$(RM_DIRS_WIN)"
ifeq ($(SYSTEM),NIX)
RM_ALL = $(RM_ALL_NIX)
RM = rm
else
RM_ALL = $(RM_ALL_WIN)
RM = del
endif

# Directory Path Make Commands
MKDIR_NIX = mkdir -p
MKDIR_WIN = $(MAKDIR) /P 
ifeq ($(SYSTEM),NIX)
MKDIR = $(MKDIR_NIX)
else
MKDIR = $(MKDIR_WIN)
endif

# Copy Commands
FCOPY_NIX := cp
FCOPY_WIN := copy 
ifeq ($(SYSTEM),NIX)
FCOPY := $(FCOPY_NIX)
else
FCOPY := $(FCOPY_WIN)
endif

# Editor Commands - Feel free to change `nano -w` to whatever editor you prefer
TXTEDITOR_NIX := nano -w
TXTEDITOR_WIN := notepad
ifeq ($(SYSTEM),NIX)
TXTEDITOR := $(TXTEDITOR_NIX)
else
TXTEDITOR := $(TXTEDITOR_WIN)
endif

# Empty Echo Commands
EECHO_NIX := echo ''
EECHO_WIN := echo.
ifeq ($(SYSTEM),NIX)
EECHO := $(EECHO_NIX)
else
EECHO := $(EECHO_WIN)
endif



# All markdown files. Recursive search with `find`
ALL_MD = $(call find_all,"*.md")

# For all known markdown files: change md extension to html and prepend the deploy directory.
ifdef SITEMENU_MD
ifdef FOOTER_MD
HTML_FROM_MD := $(patsubst %.md,%.html,$(call map,filter-out,$(SITEMENU_MD) $(FOOTER_MD),$(ALL_MD)))
ALL_MD_F := $(call map,filter-out,$(SITEMENU_MD) $(FOOTER_MD),$(ALL_MD))
else
HTML_FROM_MD := $(patsubst %.md,%.html,$(call map,filter-out,$(SITEMENU_MD),$(ALL_MD)))
ALL_MD_F := $(call map,filter-out,$(SITEMENU_MD),$(ALL_MD))
endif
else ifdef FOOTER_MD
HTML_FROM_MD := $(patsubst %.md,%.html,$(call map,filter-out,$(FOOTER_MD),$(ALL_MD)))
ALL_MD_F := $(call map,filter-out,$(FOOTER_MD),$(ALL_MD))
else
HTML_FROM_MD := $(patsubst %.md,%.html,$(ALL_MD))
ALL_MD_F := $(ALL_MD)
endif

# File types that need to be processed
PROCESS_TYPES = %.md

# File types that control processing
CONTROL_TYPES = %.tmp

# Blog entry list
ifeq ($(SYSTEM),NIX)
BLOG_ENTRIES = $(shell ls -1 -dt $(BUILD_DIRECTORY_NIX)blog$(SEP)20??-??-??_??-??-??-???.md")
else
BLOG_ENTRIES = $(shell cmd /c "dir /b /o:-n $(BUILD_DIRECTORY_WIN)\blog\20??-??-??_??-??-??-???.md")
endif

### Identity Files
# All files that should be deployed as-is
# Clean uploads first!
ifeq ($(SYSTEM),NIX)
UPLOADS := $(shell find $(UPLOAD_DIRECTORY_NIX) -name "* *" -exec sh $(RFWS) '{}' \;)
else
UPLOADS := $(shell cmd /c "$(RFFWS) /r $(UPLOAD_DIRECTORY_WIN) >NUL")
endif
# Start assuming all files
ALL = $(call find_all,"*.*") $(UPLOADS)
# Non-identity types are types we want to process
# anything in the deploy directory
# and files we want to ignore:
EXCLUDE_TYPES = $(PROCESS_TYPES) $(CONTROL_TYPES) $(DEPLOY_DIRECTORY)$(SEP)%  $(SRC_CTL)  ./%.DS_Store  %.DS_Store $(SITEMENU_MD) $(SITEMENU_HTML) $(SITEMENU_MDHTML) $(DEPLOY_DIRECTORY)% $(FOOTER_MD) $(FOOTER_HTML) $(FOOTER_MDHTML)

# filter out excluded types; everything else gets transfered as-is... "identity"
ALL_IDENTITY := $(call map,filter-out,$(EXCLUDE_TYPES),$(ALL))

# Everything that needs deploying :
# all the identity files
# and all the html files derived from markdown
ALL_TO_DEPLOY = $(ALL_IDENTITY) $(HTML_FROM_MD) $(BUILD_DIRECTORY)$(SEP)blog$(SEP)index.html
ifeq ($(SYSTEM),NIX)
DEPLOY_TARGETS := $(subst $(BUILD_DIRECTORY_NIX),$(DEPLOY_DIRECTORY_NIX),$(ALL_TO_DEPLOY))
else
DEPLOY_TARGETS := $(subst $(BUILD_DIRECTORY_WIN)\,$(DEPLOY_DIRECTORY_WIN)\,$(ALL_TO_DEPLOY))
endif

# First recipe is default. Nothing to do except dependency on all html files.
bake: $(DEPLOY_TARGETS)

# Information on variable settings, used for debugging.  You can probably ignore this section.
info:
	@echo "$(SITETITLE)";"$(SITEINFO)","$(BLOG_ENTRIES)"
	@echo "$(DEPLOY_TARGETS)","$(DEPLOY_DIRECTORY)"
	@echo --- "$(EXCLUDE_TYPES)"
	@echo === "$(ALL_MD)"
	@echo ;;; "$(ALL_TO_DEPLOY)"
	@echo ::: "$(ALL_MD_F)"-"$(HTML_FROM_MD)"

clean:
	$(RM_ALL)
	$(RM) menu.html

ifdef SITEMENU_MD
$(SITEMENU_MDHTML): $(SITEMENU_MD)
	@echo Converting menu: $<
	@$(MARKDOWN_NS_MENU) $< --output $@
endif

ifdef FOOTER_MD
$(FOOTER_MDHTML): $(FOOTER_MD)
	@echo Converting FOOTER: $<
	@$(MARKDOWN_NS_FOOTER) $< --output $@
endif

# Recipe for html files in the deploy directory for a corresponding markdown file

ifeq ($(SYSTEM),NIX)
$(DEPLOY_DIRECTORY_NIX)/%.html : $(BUILD_DIRECTORY_NIX)/%.md $(TEMPLATE) $(BUILD_DIRECTORY_NIX)$(SEP)$(CSS) $(FOOTER_MDHTML) $(SITEMENU_MDHTML)
	@echo Converting: $<
	@$(MKDIR) $(dir $@)
	@$(MARKDOWN) $< --output $@
else
$(DEPLOY_DIRECTORY_WIN)%.html : $(BUILD_DIRECTORY_WIN)%.md $(TEMPLATE) $(BUILD_DIRECTORY_WIN)$(SEP)$(CSS) $(FOOTER_MDHTML) $(SITEMENU_MDHTML)
	@echo Converting: $<
	@$(MKDIR) $(dir $@)
	@$(MARKDOWN) $< --output $@
endif

# Recipe for plain html and css files

ifeq ($(SYSTEM),NIX)
$(subst $(BUILD_DIRECTORY_NIX),$(DEPLOY_DIRECTORY_NIX),$(BUILD_DIRECTORY_NIX)/%.html): $(BUILD_DIRECTORY_NIX)/%.html
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) $< $@
else
$(subst $(BUILD_DIRECTORY_WIN)\\,$(DEPLOY_DIRECTORY_WIN)\\,$(BUILD_DIRECTORY_WIN)\\%.html): $(BUILD_DIRECTORY_WIN)\%.html
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) $< $@
endif

ifeq ($(SYSTEM),NIX)
$(subst $(BUILD_DIRECTORY_NIX)/,$(DEPLOY_DIRECTORY_NIX)/,$(BUILD_DIRECTORY_NIX)/%.css): $(BUILD_DIRECTORY_NIX)/%.css
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) $< $@
else
$(subst $(BUILD_DIRECTORY_WIN)\\,$(DEPLOY_DIRECTORY_WIN)\\,$(BUILD_DIRECTORY_WIN)\\%.css): $(BUILD_DIRECTORY_WIN)\%.css
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) $< $@
endif

# Recipe for the "upload" directory - these files are added as is!

ifeq ($(SYSTEM),NIX)
$(subst $(BUILD_DIRECTORY_NIX)/,$(DEPLOY_DIRECTORY_NIX)/,$(UPLOAD_DIRECTORY_NIX)/%): $(UPLOAD_DIRECTORY_NIX)/%
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) "$<" "$@"
else
$(subst $(BUILD_DIRECTORY_WIN)\,$(DEPLOY_DIRECTORY_WIN)\,$(UPLOAD_DIRECTORY_WIN)\\%): $(UPLOAD_DIRECTORY_WIN)\%
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) "$<" "$@"
endif

# Recipe for the "images" directory - these files may be added as is or converted with ImageMagick

ifeq ($(SYSTEM),NIX)
$(subst $(BUILD_DIRECTORY_NIX)/,$(DEPLOY_DIRECTORY_NIX)/,$(IMAGES_DIRECTORY_NIX)/%): $(IMAGES_DIRECTORY_NIX)/%
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) "$<" "$@"
else
$(subst $(BUILD_DIRECTORY_WIN)\,$(DEPLOY_DIRECTORY_WIN)\,$(IMAGES_DIRECTORY_WIN)\\%): $(IMAGES_DIRECTORY_WIN)\%
	@echo Moving $< to $@
	@$(MKDIR) $(dir $@)
	@$(FCOPY) "$<" "$@"
endif


# Recipe to assemble Blog Index page (lists all the blog entries by date

$(DEPLOY_DIRECTORY)$(SEP)blog$(SEP)index.html : templates$(SEP)blog_index.md $(BLOG_ENTRY_LIST) $(TEMPLATE_BLOG)
	$(MARKDOWN_NS_BLOGMENU) "$(BLOG_ENTRY_LIST)" --output "$(BLOG_ENTRY_LIST).html"
	$(MARKDOWN_BLOGINDEX) $< "$(BLOG_ENTRY_LIST).html"  --output $@
	$(RM) "$(BLOG_ENTRY_LIST).html"

ifeq ($(SYSTEM),NIX)
$(BLOG_ENTRY_LIST) : $(foreach a,$(BLOG_ENTRIES),$(addprefix $(BUILD_DIRECTORY_NIX)/blog/,$(a)))  $(TEMPLATE_BLOG_MENU)  $(FOOTER_MDHTML) $(SITEMENU_MDHTML)
	$(shell rm $@ >>/dev/null)$(foreach O,$(BLOG_ENTRIES),$(shell echo "- [$(wordlist 2, 100, $(shell cat $(realpath $(addprefix $(BUILD_DIRECTORY_NIX)/blog/,$O)) | sed -n 1p )): $(subst .md,,$O)]($(subst .md,.html,$O))"  >>$@ ))
	@echo Blog Entries: $@
else
$(BLOG_ENTRY_LIST) : $(foreach a,$(BLOG_ENTRIES),$(addprefix $(BUILD_DIRECTORY_WIN)\blog\,$(a)))  $(TEMPLATE_BLOG_MENU)  $(FOOTER_MDHTML) $(SITEMENU_MDHTML)
	$(shell del $@ >>NUL)$(foreach O,$(BLOG_ENTRIES),$(shell cmd /c "echo - [$(wordlist 2, 100, $(shell cmd /c "$(GETLINES) $(realpath $(addprefix $(BUILD_DIRECTORY_WIN)\blog\,$O)) 1, ")): $(subst .md,,$O)]($(subst .md,.html,$O))"  >>$@ ))
	@echo Blog Entries: $@
endif

# Recipe for making new blog entries!  Use `make new-blog` to invoke.

ifeq ($(SYSTEM),NIX)
CURRENT_DATETIME := $(shell date "+%04Y-%02m-%02d %02H:%02M:%02S.000")
CURRENT_DATETIMEF := $(shell date "+%04Y-%02m-%02d_%02H-%02M-%02S-000")
else
CURRENT_DATETIME := $(shell $(ISODATE))
CURRENT_DATETIMEF := $(shell $(ISODATE) /F)
endif

new-blog:
	@echo Creating new blog entry $(CURRENT_DATETIMEF)
	@echo %% Blog entry for $(CURRENT_DATETIME) >>"$(BUILD_DIRECTORY)$(SEP)blog$(SEP)$(CURRENT_DATETIMEF).md"
	@echo %% Author's Name >>"$(BUILD_DIRECTORY)$(SEP)blog$(SEP)$(CURRENT_DATETIMEF).md"
	@echo %% $(CURRENT_DATETIME) >>"$(BUILD_DIRECTORY)$(SEP)blog$(SEP)$(CURRENT_DATETIMEF).md"
	@$(EECHO) >>"$(BUILD_DIRECTORY)$(SEP)blog$(SEP)$(CURRENT_DATETIMEF).md"
	$(TXTEDITOR) "$(BUILD_DIRECTORY)$(SEP)blog$(SEP)$(CURRENT_DATETIMEF).md"


# Recipes to deploy to Github Pages.  Retained from the original "Bake" by Fabian Canas - I never bothered using these.

REMOTE = github
BRANCH = gh-pages

deploy: bake
	git add $(DEPLOY)
	git commit -m 'Deploy'
	git subtree push --prefix=$(DEPLOY) $(REMOTE) $(BRANCH)

undeploy:
	git push $(REMOTE) `git subtree split --prefix $(DEPLOY) $(BRANCH)`:$(BRANCH) --force
```
